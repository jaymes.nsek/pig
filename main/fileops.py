
"""
An interfacing functions between Dictionary and FileObjects.

Dictionarys are used to register changes to data, so that, FileObjects are
only accessed when data has changed during the App's active cycle.
"""

from io import TextIOWrapper
from pig.main.enums_module import PlayMode
from pig.main.enums_module import FirstMover
from pig.main.enums_module import PlayerObj
from pig.main.enums_module import Difficulty
from pig.main.enums_module import FileMode
from pig.main.enums_module import DataKey


def get_file_object(filename, mode):
    """
    Open a file for READ, WRITE or APPEND operation.

    It is imperative the caller calls close() on file_object after use.
    @ filename is the name or path of the file
    @ return file_object or None if it does not exist
    @ raises an OSError if the file cannot be opened
    """
    if not isinstance(mode, FileMode):
        raise TypeError('mode should be an Enum of type FileMode')
    file_object = None
    try:
        file_object = open(filename, mode.value)
    except (FileNotFoundError, OSError):
        print(str(filename) + ' failed to open in ' + str(mode) + 'mode.')
    return file_object

def read_file_to_dict(file_object):
    """
    Read content from file object to dict, and return the dict.

    Load each record from file to a dict object, each record has two parts
    with ':' as delimiter, e.g. 'score:name'. The first part is used as
    dict key and so needs to be unique. Auto-closes file_object after use.

    @ param file_object is the return value of open(a_filename, 'r')
    @ return a Dictionary of the key value pair records of the file
    @ raises TypeError of file_object None or not type TextIOWrapper
    """
    if not is_file_object(file_object):
        raise TypeError('file_object param is not of required type.')
    a_dict = dict()
    for line in file_object:
        line = line.strip()
        if line.count(':') == 1:
            line_list = line.split(':')
            a_dict[line_list[0]] = line_list[1]
    close_file_object(file_object)
    return a_dict

def write_dict_to_file(file_object, a_dict):
    """
    Write dict contents to file, and auto-close file_object after use.

    Used ':' as delimiter, in the format 'key:value'.

    @ param file_object of type TextIOWrapper and in WRITE mode
    @ param a_dict is a Dictionary object
    @ raises TypeError if file_object is not type TextIOWrapper or
             a_dict is not type Dict, or either is None.
             RuntimeError if file_object is not in WRITE mode.
    @ return True if no error is encountered
    """
    # Enforce data types
    is_not_dict = not isinstance(a_dict, dict)
    is_null = file_object is None or a_dict is None
    if is_null or not is_file_object(file_object) or is_not_dict:
        raise TypeError('Params do not satify type requirement')
    if file_object.mode != FileMode.WRITE.value:
        raise RuntimeError(file_object.name + ' is not in WRITE mode.')
    for key, value in zip(a_dict.keys(), a_dict.values()):
        data = str(key) + ":" + str(value) + "\n"
        file_object.write(data)
    close_file_object(file_object)
    return True

def is_file_object(file_object):
    """Return True if object is a FileObject, otherise False."""
    return isinstance(file_object, TextIOWrapper)

def dict_contains(a_dict, key_list):
    """Return true if dictionary contains all and only the keys in list."""
    sym_set = set(key_list).symmetric_difference(a_dict.keys())
    return len(sym_set) == 0

def close_file_object(file_object):
    """Close FileObject if it is still alive."""
    if isinstance(file_object, TextIOWrapper) and not file_object.closed:
        file_object.close()
